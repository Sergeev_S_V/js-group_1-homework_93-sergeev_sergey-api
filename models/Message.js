const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
  title: {
    type: String, required: true
  },
  sender: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  participant: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

const Message = mongoose.model('Message', MessageSchema);

module.exports = Message;