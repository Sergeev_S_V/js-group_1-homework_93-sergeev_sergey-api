const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');
const Message = require('./models/Message');
const users = require('./app/users');
const User = require('./models/User');

const app = express();

const expressWs = require('express-ws')(app);

const port = 8000;

app.use(cors());
app.use(express.json());

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

const clients = {};
let connectedUsers;

db.once('open', () => {
  console.log('Mongoose connected!');

  app.ws('/chat', async (ws, req) => {
    const token = req.query.token;

    const user = await User.findOne({token});

    if (!user) {
      ws.send(JSON.stringify({
        type: 'Error',
        message: 'Not authenticated'
      }));
      return ws.close();
    }

    const id = req.get('sec-websocket-key');
    clients[id] = ws;
    clients[id].connectedUser = user;

    Object
      .values(clients)
      .forEach(client => (
        client.send(JSON.stringify({
          type: 'USER_LOGGED_IN',
          user: clients[id].connectedUser
        }))
      ));

    connectedUsers =
      Object
        .keys(clients)
        .map(key => (clients[key].connectedUser));

    ws.send(JSON.stringify({
      type: 'CONNECTED_USERS',
      connectedUsers
    }));

    const messages = await Message
      .find()
      .populate('sender')
      .populate('participant');

    ws.send(JSON.stringify({
      type: 'ALL_MESSAGES',
      messages: messages.slice(-30),
    }));

    ws.on('message', async msg => {
      let decodedMessage;
      try {
        decodedMessage = JSON.parse(msg);
      } catch (e) {
        return ws.send(JSON.stringify({
          type: 'ERROR',
          message: 'Message is not JSON'
        }));
      }

      switch (decodedMessage.type) {
        case 'CREATE_MESSAGE':
          const messageData = {
            title: decodedMessage.message,
            sender: user._id
          };
          const newMessage = new Message(messageData);
          await newMessage.save();

          const message = await Message
            .findOne({_id: newMessage._id})
            .populate({path: 'sender participant', select: 'username role'});

          Object
            .values(clients)
            .forEach(client => (
              client.send(JSON.stringify({
                type: 'NEW_MESSAGE',
                message: message
              }))
            ));
          break;
        case 'DELETE_MESSAGE':
          if (user.role === 'moderator') {
            const id = decodedMessage.messageId;
            await Message.remove({_id: id});

            const messages = await Message.find().populate('sender');
            ws.send(JSON.stringify({
              type: 'ALL_MESSAGES',
              messages: messages
            }));
          }
          break;
        case 'PRIVATE_MESSAGE':
          const messageInfo = {
            title: decodedMessage.message,
            sender: decodedMessage.sender,
            participant: decodedMessage.participant
          };

          const savingMessage = new Message(messageInfo);
          await savingMessage.save();

          Object
            .values(clients)
            .forEach(client => (
              client.send(JSON.stringify({
                type: 'NEW_MESSAGE',
                message: messageInfo
              }))
            ));

          break;
        default:
          return ws.send(JSON.stringify({
            type: 'ERROR',
            message: 'Unknown message type'
          }));
      }

    });

    ws.on('close', (msg) => {
      delete clients[id];
      connectedUsers =
        Object
          .keys(clients)
          .map(key => (clients[key].connectedUser));

      Object
        .values(clients)
        .forEach(client => (
          client.send(JSON.stringify({
            type: 'USER_LOGGED_OUT',
            connectedUsers
          }))
        ));
    });

  });

  app.use('/users', users());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
